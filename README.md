# gometie

Package `gometie` is a Golang library for talking to the [met.ie](https://met.ie) JSON API.
It can also be used standalone.

Example usage:

```go
package main

import (
    "log"
    "gitlab.com/johnstcn/gometie/pkg/met"
)

func main() {
    metClient := met.New()
    stations, err := metClient.Stations()
    if err != nil {
        log.Fatal(err)
    }
	for _, s := range stations {
		log.Println("fetching weather data for", s)
		stationReports, err := metClient.HourlyReports(s.Slug)
		if err != nil {
			log.Printf("fetching reports for station %s\n", err)
		}
		for _, r := range stationReports {
			log.Printf("rainfall for %s at %s: %0.2f\n", r.Name, r.Date, r.Rainfall)
		}
	}
}

```