package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"

	"github.com/pkg/errors"
	"gitlab.com/johnstcn/gometie/pkg/met"
)

func main() {
	client := met.New()
	var station string
	flag.StringVar(&station, "station", "", "MET station to query")
	flag.Parse()
	if len(station) == 0 {
		stations, err := client.Stations()
		if err != nil {
			fmt.Println(errors.Wrap(err, "querying stations from met.ie"))
			os.Exit(1)
		}

		fmt.Println("provide at least one station from:")
		for _, s := range stations {
			fmt.Println("\t", s.Slug)
		}
		os.Exit(1)
	}

	reports, err := client.HourlyReports(station)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	prettyJSON, err := json.MarshalIndent(reports, "", "\t")
	if err != nil {
		fmt.Println("prettifying json", err)
		os.Exit(1)
	}

	fmt.Printf("%s\n", string(prettyJSON))
}
