module gitlab.com/johnstcn/gometie

go 1.12

require (
	github.com/gosimple/slug v1.9.0
	github.com/hashicorp/go-cleanhttp v0.5.1
	github.com/pkg/errors v0.8.1
)
