package met

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/hashicorp/go-cleanhttp"

	"github.com/gosimple/slug"
	"github.com/pkg/errors"
)

var (
	// metBaseUrl is the base URL for the met.ie JSON API
	metBaseUrl          = "https://www.met.ie/api"
	metStationCoordsUrl = metBaseUrl + "/stations/coordinates"
	metStationUrl       = metBaseUrl + "/stations"
)

// hourlyReportResponse describes the stringly-typed hourly report response from met.ie
type hourlyReportResponse struct {
	Name               string `json:"name"`
	Temperature        string `json:"temperature"`
	Symbol             string `json:"symbol"`
	WeatherDescription string `json:"weatherDescription"`
	Text               string `json:"text"`
	WindSpeed          string `json:"windSpeed"`
	// WindGust string `json:"windGust"` // TODO: mostly reported as "-" by API
	CardinalWindDirection string `json:"cardinalWindDirection"`
	WindDirection         int    `json:"windDirection"`
	Humidity              string `json:"humidity"`
	Rainfall              string `json:"rainfall"`
	Pressure              string `json:"pressure"`
	DayName               string `json:"dayName"`
	Date                  string `json:"date"`
	ReportTime            string `json:"reportTime"`
}

type HourlyReport struct {
	// Name is the name of the weather station recorded
	Name string
	// Temperature is the recorded temperature (°C)
	Temperature int
	// TODO figure out what symbol means
	//Symbol string
	// WeatherDescription is a human-readable weather description
	WeatherDescription string
	// Text is the quoted weather description
	Text string
	// WindSpeed is the recorded wind speed (km/h)
	WindSpeed int
	// WindGust *int `json:"windGust"` // TODO: mostly reported as "-" by API
	CardinalWindDirection string
	// WindDirection is the currently measured wind direction in degrees (0-359)
	WindDirection int
	// Humidity is the measured humidity (g/m3)
	Humidity int
	// Rainfall is the amount of precipitation measured (mm)
	Rainfall float64
	// Pressure is the air pressure measured (hPa)
	Pressure int
	// Date is the date of the set of measurements (DD-MM-YYYY)
	Date time.Time
}

type StationInfo struct {
	Name string  `json:"station"`
	Slug string  `json:"slug"`
	Lat  float64 `json:"lat"`
	Lng  float64 `json:"lng"`
}

// ToHourlyReport converts an hourlyReportResponse to an HourlyReport
func (r hourlyReportResponse) ToHourlyReport() HourlyReport {
	var h HourlyReport
	h.Name = r.Name
	h.WeatherDescription = r.WeatherDescription
	h.Text = r.Text
	h.CardinalWindDirection = r.CardinalWindDirection
	h.WindDirection = r.WindDirection
	h.Temperature, _ = strconv.Atoi(strings.TrimSpace(r.Temperature))
	h.WindSpeed, _ = strconv.Atoi(strings.TrimSpace(r.WindSpeed))
	h.Humidity, _ = strconv.Atoi(strings.TrimSpace(r.Humidity))
	h.Rainfall, _ = strconv.ParseFloat(strings.TrimSpace(r.Rainfall), 64)
	h.Pressure, _ = strconv.Atoi(strings.TrimSpace(r.Pressure))
	h.Date, _ = time.Parse("02-01-2006 15:04", r.Date+" "+r.ReportTime)
	return h
}

// Getter is something that can perform a GET request
type Getter interface {
	Get(url string) (*http.Response, error)
}

// *http.Client satisfies the Getter interface
var _ Getter = (*http.Client)(nil)

// Client is a http for met.ie hourly reports
type Client interface {
	// HourlyReports returns today's' hourly reports for Station where
	HourlyReports(where string) ([]HourlyReport, error)
	// Stations returns the list of known stations from met.ie
	Stations() ([]StationInfo, error)
}

// hourlyMetClient implements the Client interface.
var _ Client = (*hourlyMetClient)(nil)

func New() Client {
	return &hourlyMetClient{http: cleanhttp.DefaultClient()}
}

type hourlyMetClient struct {
	http Getter
}

func (c *hourlyMetClient) HourlyReports(where string) ([]HourlyReport, error) {
	rawReports := make([]hourlyReportResponse, 0, 24)
	url := metStationUrl + fmt.Sprintf("/%s/today", slug.Make(where))
	log.Println("GET", url)
	resp, err := c.http.Get(url)
	if err != nil {
		return nil, errors.Wrapf(err, "get %s", url)
	}

	defer func() {
		if err := resp.Body.Close(); err != nil {
			log.Println(errors.Wrap(err, "closing response body"))
		}
	}()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "reading %s", url)
	}

	if err := json.Unmarshal(body, &rawReports); err != nil {
		return nil, errors.Wrap(err, "unmarshaling response body")
	}

	reports := make([]HourlyReport, 0, len(rawReports))
	for _, r := range rawReports {
		reports = append(reports, r.ToHourlyReport())
	}
	return reports, nil
}

func (c *hourlyMetClient) Stations() ([]StationInfo, error) {
	stations := make([]StationInfo, 0)
	log.Println("GET", metStationCoordsUrl)
	resp, err := c.http.Get(metStationCoordsUrl)
	if err != nil {
		return nil, errors.Wrapf(err, "get %s", metStationCoordsUrl)
	}

	if resp.StatusCode != http.StatusOK {
		log.Printf("expected OK status but got %d\n", resp.StatusCode)
	}

	defer func() {
		if err := resp.Body.Close(); err != nil {
			log.Println(errors.Wrap(err, "closing response body"))
		}
	}()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrapf(err, "reading %s", metStationCoordsUrl)
	}

	if err := json.Unmarshal(body, &stations); err != nil {
		return nil, errors.Wrap(err, "unmarshaling response body")
	}

	return stations, nil
}
