package met

import (
	"reflect"
	"testing"
)

func TestNew(t *testing.T) {
	tests := []struct {
		name string
		want Client
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := New(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_hourlyMetClient_HourlyReports(t *testing.T) {
	type fields struct {
		http Getter
	}
	type args struct {
		where string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []HourlyReport
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &hourlyMetClient{
				http: tt.fields.http,
			}
			got, err := c.HourlyReports(tt.args.where)
			if (err != nil) != tt.wantErr {
				t.Errorf("HourlyReports() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("HourlyReports() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_hourlyMetClient_Stations(t *testing.T) {
	type fields struct {
		http Getter
	}
	tests := []struct {
		name    string
		fields  fields
		want    []StationInfo
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &hourlyMetClient{
				http: tt.fields.http,
			}
			got, err := c.Stations()
			if (err != nil) != tt.wantErr {
				t.Errorf("Stations() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Stations() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_hourlyReportResponse_ToHourlyReport(t *testing.T) {
	type fields struct {
		Name                  string
		Temperature           string
		Symbol                string
		WeatherDescription    string
		Text                  string
		WindSpeed             string
		CardinalWindDirection string
		WindDirection         int
		Humidity              string
		Rainfall              string
		Pressure              string
		DayName               string
		Date                  string
		ReportTime            string
	}
	tests := []struct {
		name   string
		fields fields
		want   HourlyReport
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := hourlyReportResponse{
				Name:                  tt.fields.Name,
				Temperature:           tt.fields.Temperature,
				Symbol:                tt.fields.Symbol,
				WeatherDescription:    tt.fields.WeatherDescription,
				Text:                  tt.fields.Text,
				WindSpeed:             tt.fields.WindSpeed,
				CardinalWindDirection: tt.fields.CardinalWindDirection,
				WindDirection:         tt.fields.WindDirection,
				Humidity:              tt.fields.Humidity,
				Rainfall:              tt.fields.Rainfall,
				Pressure:              tt.fields.Pressure,
				DayName:               tt.fields.DayName,
				Date:                  tt.fields.Date,
				ReportTime:            tt.fields.ReportTime,
			}
			if got := r.ToHourlyReport(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ToHourlyReport() = %v, want %v", got, tt.want)
			}
		})
	}
}
